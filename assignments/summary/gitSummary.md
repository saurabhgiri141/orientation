# Git Summary
Git is version-control software that makes collaboration with teammates super simple and it lets you easily keep track of every revision you and your team make during the development of your software.

![git1](/uploads/ac9d4f5d58da02a57f3c496f8d3bc614/git1.png)

## Required Vocabulary
- **Repository** is the collection of files and folders that you’re using git to track.
- **Commit** saves your work in thee Repository.
- **Push** means syncing your commits.
- **Branch** are the separate instance of the main code.
- **Merge** means integrating the branch with main code.
- **Clone** means making a copy of main code into local machine
- **Fork** means making a copy of main code in your Repositorywith ypur username.

![WhatsApp_Image_2020-09-27_at_3.47.12_PM](/uploads/122af9de654468f0f7b03d8db0fcb615/WhatsApp_Image_2020-09-27_at_3.47.12_PM.jpeg)

## Getting Started and Required Commands
### How to Install Git
**For Linux**: sudo apt-get install git

**For Windows**: Download the git installer and run it
### Git Internals
- 3 main states of files:
 **Modified** means changed the file but not committed.
 **Staged** means marked the changes but not committed yet.
 **Committed** means that changes have been saved to repository
- **Workspace** is the tree of repo in which you make all the changes through Editor(s). **Staging** is the repo in which all the staged files are saved. **Local Repository** is where all the committed files are saved. **Remote Repository** is the copy of local repo on some server, changes made on loacl repo doesn't affect this repo.

![git3](/uploads/6f78b9fa888bdc9e4e776078a63b0475/git3.png)
## Git Workflow
- **Clone repo**:$ git clone (link to repository) 
- **Create new branch**:$ git checkout master
                        $ git checkout -b (your branch name)
- **Staging changes**:$ git add .
- **Commit changes**:$ git commit -sv
- **Push Files**:$ git push origin (branch name)

![git4](/uploads/6116f205f9b74be3055e60d366e96e7b/git4.png)
